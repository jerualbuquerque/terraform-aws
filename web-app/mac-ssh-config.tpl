cat << EOF >> /Users/jerualbuquerque/.ssh/config

    Host ${hostname}
    User ${user}
    IdentityFile ${identityfile}
EOF