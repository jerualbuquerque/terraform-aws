terraform {
  #############################################################
  ## AFTER RUNNING TERRAFORM APPLY (WITH LOCAL BACKEND)
  ## YOU WILL UNCOMMENT THIS CODE THEN RERUN TERRAFORM INIT
  ## TO SWITCH FROM LOCAL BACKEND TO REMOTE AWS BACKEND
  #############################################################
  backend "s3" {
    bucket = "devops-jeru-tf-state" # REPLACE WITH YOUR BUCKET NAME
    key    = "web-app/terraform.tfstate"
    region = "us-east-1"
    #dynamodb_table = "terraform-state-locking"
    #encrypt        = true
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>4.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_vpc" "jeru_vpc" {
  cidr_block           = "10.123.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "dev"
  }


}

resource "aws_subnet" "jeru_public_subnet" {
  vpc_id                  = aws_vpc.jeru_vpc.id
  cidr_block              = "10.123.1.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1a"

  tags = {
    Name = "dev-public"
  }

}

resource "aws_internet_gateway" "jeru_internet_gateway" {
  vpc_id = aws_vpc.jeru_vpc.id

  tags = {
    Name = "jeru_igw"
  }
}

resource "aws_route_table" "jeru_public_rt" {
  vpc_id = aws_vpc.jeru_vpc.id

  tags = {
    Name = "jeru_dev_rt"
  }

}

resource "aws_route" "default_route" {
  route_table_id         = aws_route_table.jeru_public_rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.jeru_internet_gateway.id

}

resource "aws_route_table_association" "jeru_rt_assoc" {
  subnet_id      = aws_subnet.jeru_public_subnet.id
  route_table_id = aws_route_table.jeru_public_rt.id
}

resource "aws_security_group" "jeru_sg" {
  name        = "dev_sg"
  description = "dev security group"
  vpc_id      = aws_vpc.jeru_vpc.id
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]

  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_key_pair" "jeru_auth" {
  key_name   = "jeru_key"
  public_key = file("/Users/jerualbuquerque/.ssh/jeruaws.pub")

}

resource "aws_instance" "dev_node1" {
  instance_type          = "t2.micro"
  ami                    = data.aws_ami.server_ami.id
  key_name               = aws_key_pair.jeru_auth.id
  vpc_security_group_ids = [aws_security_group.jeru_sg.id]
  subnet_id              = aws_subnet.jeru_public_subnet.id
  user_data              = file("/Users/jerualbuquerque/Documents/Terraform_Course/my_devops_course/web-app/userdatajenkins.tpl")

  root_block_device {
    volume_size = 10
  }
  tags = {
    Name = "dev-node"

  }

  provisioner "local-exec" {
    command = templatefile("mac-ssh-config.tpl", {
      hostname = self.public_ip,
      user = "ubuntu",
      identityfile = "/Users/jerualbuquerque/.ssh/jeruaws"
    })
    interpreter = var.host_os == "macos" ? ["bash", "-c"] : ["Powershell" , "-command"]
  }


}








